package com.wrel.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wrel.apiservice.IFindRegistrationService;
import com.wrel.vo.output.FindRegistrationData;

@Controller
public class APIController {

	@Autowired
	private IFindRegistrationService findRegistrationService;

	@RequestMapping("/FindRegistrationData")
	public @ResponseBody FindRegistrationData findWording(HttpServletRequest request, HttpServletResponse response) {
		return findRegistrationService.genResponseData(request, response, FindRegistrationData.class);
	}

}
