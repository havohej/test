package com.wrel.enums;

public enum ExceptionLevel {
	DEBUG, INFO, WARN, ERROR;
}
