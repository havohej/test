package com.wrel.enums;

public enum StatusCode {

	SUCCESS("0", "成功"),
	NO_DATA("1", "無資料"),
	CHECK_CHL_CHK_FAIL("11", "檢查 chl/chk 失敗"),
	VALIDATE_PARAMS_FAIL("12", "驗證參數錯誤"),
	FIND_DB_DATA_FAIL("13", "取得DB對應資料錯誤"),
	FIND_XML_DATA_FAIL("14", "取得XML資料失敗"),
	SYSTEM_ERROR("99", "系統錯誤"),
	;

	private String code;
	private String description;

	public static StatusCode findByCode(String code) {
		StatusCode result = null;
		for (StatusCode statusCode : StatusCode.values()) {
			if (statusCode.getCode().equals(code)) {
				result = statusCode;
				break;
			}
		}
		return result;
	}

	StatusCode(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

}
