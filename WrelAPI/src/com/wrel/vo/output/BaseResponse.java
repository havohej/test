package com.wrel.vo.output;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public class BaseResponse<T> {

	@XmlAttribute
	final String name = this.getClass().getSimpleName();

	Status status;
	T data;

	public String getName() {
		return name;
	}

	public Status getStatus() {
		return status;
	}

	@XmlElement
	public void setStatus(Status status) {
		this.status = status;
	}

	public T getData() {
		return data;
	}

	@XmlElement
	public void setData(T data) {
		this.data = data;
	}

}
