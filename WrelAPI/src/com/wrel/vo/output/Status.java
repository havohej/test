package com.wrel.vo.output;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.wrel.enums.StatusCode;

@XmlRootElement
@XmlType(propOrder = { "code", "message" })
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Status {

	String code;
	String message;

	public Status() {

	}

	public Status(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public Status(StatusCode statusCode) {
		this.code = statusCode.getCode();
		this.message = statusCode.getDescription();
	}

	public void changeCodeAndStatus(StatusCode statusCode) {
		this.code = statusCode.getCode();
		this.message = statusCode.getDescription();
	}

	public String getCode() {
		return code;
	}

	@XmlElement
	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	@XmlElement
	public void setMessage(String message) {
		this.message = message;
	}

}
