package com.wrel.vo.output;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "service")
@XmlType(propOrder = { "status", "data" })
public class FindRegistrationData extends BaseResponse<RegistrationData> {

	@Override
	public RegistrationData getData() {
		return data;
	}

	@Override
	public void setData(RegistrationData data) {
		this.data = data;
	}

}
