package com.wrel.vo.output;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlType(propOrder = { "userId", "hospital", "division", "time", "order", "recordStatus" })
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class RegistrationData {

	private String userId; // 使用者ID
	private String hospital; // 醫院
	private String division; // 科別
	private String time; // 時段
	private String order; // 號次
	private String recordStatus;// 狀態

	public String getUserId() {
		return userId;
	}

	@XmlElement
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getHospital() {
		return hospital;
	}

	@XmlElement
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getDivision() {
		return division;
	}

	@XmlElement
	public void setDivision(String division) {
		this.division = division;
	}

	public String getTime() {
		return time;
	}

	@XmlElement
	public void setTime(String time) {
		this.time = time;
	}

	public String getOrder() {
		return order;
	}

	@XmlElement
	public void setOrder(String order) {
		this.order = order;
	}

	public String getRecordStatus() {
		return recordStatus;
	}

	@XmlElement
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

}
