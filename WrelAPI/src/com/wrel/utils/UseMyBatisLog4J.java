package com.wrel.utils;

import org.apache.ibatis.logging.LogFactory;

public class UseMyBatisLog4J {

	public UseMyBatisLog4J() {
		// 只有dev打開	//TODO:不work，要找原因
		if ("dev".equals(System.getProperty("app.lifecycle"))) {
			LogFactory.useLog4JLogging();
		}
	}
}