package com.wrel.utils;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Util {

	private static final Log log = LogFactory.getLog(Util.class);

	public static void showParams(HttpServletRequest req) {
		Enumeration<String> keys = req.getParameterNames();
		String str = "";
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if ("msisdn".equalsIgnoreCase(key)) {
				sb.append(key + "=" + maskMsisdn(req.getParameter(key)) + ", ");
			} else if ("email".equalsIgnoreCase(key)) {
				sb.append(key + "=" + maskEmail(req.getParameter(key)) + ", ");
			} else {
				sb.append(key + "=" + req.getParameter(key) + ", ");
			}
		}
		if (sb.length() > 2) {
			str = sb.substring(0, sb.length() - 2) + "}";
		} else {
			str = sb.toString() + "}";
		}
		log.debug(findAPIName(req) + ":" + str);
	}

	public static String findAPIName(HttpServletRequest request) {
		String uri = request.getRequestURI();
		return uri.substring(uri.lastIndexOf("/") + 1);
	}

	public static String maskMsisdn(String src) {
		String ret = null;
		if (src != null && src.length() == 10) {
			ret = src.substring(0, 5) + "***" + src.substring(8);
		}
		return ret;
	}

	public static String maskEmail(String src) {
		String ret = null;
		if (src != null) {
			int idx = src.indexOf("@");
			if (idx > 0) {
				ret = maskChars(src.substring(0, idx), 5, 2) + "@" + maskChars(src.substring(idx + 1), 5, 2);
			}
		}
		return ret;
	}

	public static String maskChars(String src, int charCount, int maskCount) {
		int plainCount = charCount - maskCount;
		StringBuffer strBuf = new StringBuffer();
		if (src != null && src.length() > 0) {
			for (int i = 0; i < src.length(); i++) {
				if (i % charCount < plainCount) {
					strBuf.append(src.charAt(i));
				} else {
					strBuf.append('*');
				}
			}
		}
		return strBuf.toString();
	}

}
