package com.wrel.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface IRegistrationDAO {
	
	List<Map<String, Object>> findRegistrationByUserId(@Param("userId") String userId);

}
