package com.wrel.exception;

import com.wrel.enums.ExceptionLevel;
import com.wrel.enums.StatusCode;

public class WrelException extends Exception {

	private static final long serialVersionUID = 1L;

	private StatusCode statusCode;
	private ExceptionLevel level;

	public WrelException(String message) {
		super(message);
	}

	public WrelException(String message, StatusCode statusCode) {
		super(message);
		this.statusCode = statusCode;
		this.level = ExceptionLevel.ERROR;
	}

	public WrelException(String message, StatusCode statusCode, ExceptionLevel level) {
		super(message);
		this.statusCode = statusCode;
		this.level = level;
	}

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public ExceptionLevel getLevel() {
		return level;
	}

}
