package com.wrel.apiservice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wrel.vo.output.FindRegistrationData;

public interface IFindRegistrationService {

	FindRegistrationData genResponseData(HttpServletRequest request, HttpServletResponse response,
			Class<FindRegistrationData> clazz);

}
