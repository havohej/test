package com.wrel.apiservice.impl;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wrel.enums.ExceptionLevel;
import com.wrel.enums.StatusCode;
import com.wrel.exception.WrelException;
import com.wrel.utils.Util;
import com.wrel.vo.output.BaseResponse;
import com.wrel.vo.output.Status;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

@Service
public abstract class BaseAPIService<T, V extends BaseResponse<T>> {

	private static final Log log = LogFactory.getLog(BaseAPIService.class);

	// Status status; //xml的status
	T data; // xml的data, ex: MyVideoList
	V responseData; // 整個xml資料 ex: FindMyVideoList

	public static final String STATUS_KEY = "STATUS";

	/**
	 * 依據是否使用cache，來判斷要從cache取得資料還是重新建立 response data 繼承的 class 的 interface
	 * 會有這個method
	 */
	public V genResponseData(HttpServletRequest request, HttpServletResponse response, Class<V> clazz) {

		long start = System.currentTimeMillis();

		V returnVo = null;

		String chl = StringUtils.stripToEmpty(request.getParameter("chl"));
		// String chk = StringUtils.stripToEmpty(request.getParameter("chk"));
		String forceGenCacheStr = StringUtils.stripToEmpty(request.getParameter("forceGenCache"));
		String apiName = findAPIName(request);
		String showParams = showParams(request);

		log.debug("start " + apiName + " from " + chl + ", " + showParams);

		boolean forceGenCache = ("Y".equals(forceGenCacheStr)) ? true : false;

		try {
			returnVo = clazz.newInstance();

			if (useCache) {
				returnVo = findCache(request, response, forceGenCache, clazz);
			} else {
				returnVo = genResponseTmpData(request, response, clazz);
			}
		} catch (Exception e) {
			if (returnVo != null) {
				returnVo.setStatus(new Status(StatusCode.SYSTEM_ERROR.getCode(), ExceptionUtils.getMessage(e)));
				returnVo.setData(null);
			}
			log.error(ExceptionUtils.getMessage(e));
		}

		long spendTime = System.currentTimeMillis() - start;
		if (spendTime > 2000) {
			log.warn("end " + apiName + "(getCache:" + isGetCache + ") from " + chl + ": spend time " + spendTime
					+ " ms, " + showParams);
		} else {
			log.debug("end " + apiName + "(getCache:" + isGetCache + ") from " + chl + ": spend time " + spendTime
					+ " ms, " + showParams);
		}

		return returnVo;

	}

	/**
	 * 建立整個xml response data
	 */
	public V genResponseTmpData(HttpServletRequest request, HttpServletResponse response, Class<V> clazz) {

		initData(request, response);

		V returnVo = null;
		try {
			returnVo = clazz.newInstance();
			returnVo.setStatus((Status) request.getAttribute(STATUS_KEY));
			returnVo.setData(getData());
		} catch (Exception e) {
			log.error(ExceptionUtils.getMessage(e));
		}
		return returnVo;
	}

	/**
	 * init ，產生status及data 主要呼叫abstract method: findData(request)，由繼承的class
	 * Override
	 */
	void initData(HttpServletRequest request, HttpServletResponse response) {
		Status status = null;

		try {
			request.setAttribute(STATUS_KEY, status = new Status());

			data = findData(request);

			changeStatus(status);
		} catch (WrelException me) {
			request.setAttribute(STATUS_KEY, status = new Status(me.getStatusCode().getCode(), me.getMessage()));

			if (!showExceptionMessge) {
				status.setMessage(defaultExceptionMessage);
			}
			data = null;

			ExceptionLevel level = me.getLevel();
			if (level == ExceptionLevel.DEBUG) {
				log.debug(me.getMessage());
			} else if (level == ExceptionLevel.INFO) {
				log.info(me.getMessage());
			} else if (level == ExceptionLevel.WARN) {
				log.warn(me.getMessage());
			} else if (level == ExceptionLevel.ERROR) {
				log.error(ExceptionUtils.getStackTrace(me));
			}

			changeErrorMsg(me, status);
		} catch (Exception e) {
			request.setAttribute(STATUS_KEY,
					status = new Status(StatusCode.SYSTEM_ERROR.getCode(), ExceptionUtils.getMessage(e)));

			if (!showExceptionMessge) {
				status.setMessage(defaultExceptionMessage);
			}
			data = null;
			log.error(ExceptionUtils.getStackTrace(e));
		}
	}

	void changeStatus(Status status) throws Exception {
		StatusCode statusCode = StatusCode.SUCCESS;
		status.setCode(statusCode.getCode());
		status.setMessage(statusCode.getDescription());
	}

	/**
	 * 繼承的class主要需要Override的部份
	 */
	abstract T findData(HttpServletRequest request) throws Exception;

	public Status getStatus(HttpServletRequest request) {
		return (Status) request.getAttribute(STATUS_KEY);
	}

	public T getData() {
		return data;
	}

	protected String findAPIName(HttpServletRequest request) {
		String uri = request.getRequestURI();
		return uri.substring(uri.lastIndexOf("/") + 1);
	}

	/**
	 * for資安，設定是否遮蔽輸入參數，預設 false
	 */
	boolean isPixelateParam = false;

	public void setPixelateParam(boolean isPixelateParam) {
		this.isPixelateParam = isPixelateParam;
	}

	/**
	 * for資安，設定遮蔽輸入參數為true時，需要遮蔽的欄位
	 */
	String[] pixelateParams() {
		return new String[] {};
	}

	private String showParams(HttpServletRequest request) {
		Enumeration<String> keys = request.getParameterNames();
		String str = "";
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		String[] pixelateParams = pixelateParams();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (!("chl".equals(key) || "chk".equals(key))) {
				String value = request.getParameter(key);
				if (isPixelateParam && ArrayUtils.contains(pixelateParams, key)) {
					value = Util.maskChars(value, 3, 2);
				}
				sb.append(key + "=" + value + ", ");
			}

			if (sb.length() > 2) {
				str = sb.substring(0, sb.length() - 2) + "}";
			} else {
				str = sb.toString() + "}";
			}
		}
		return str;
	}

	// ------------- 訊息相關 -------------\\

	/**
	 * status是否顯示 ExceptionUtils.getMessage(e) false 的話只顯示
	 * defaultExceptionMessage
	 */
	boolean showExceptionMessge = true;

	String defaultExceptionMessage = StatusCode.SYSTEM_ERROR.getDescription();

	public void setShowExceptionMessge(boolean showExceptionMessge) {
		this.showExceptionMessge = showExceptionMessge;
	}

	public void setDefaultExceptionMessage(String defaultExceptionMessage) {
		this.defaultExceptionMessage = defaultExceptionMessage;
	}

	void changeErrorMsg(WrelException me, Status status) {

	}

	// ------------- cache 相關 -------------\\

	@Autowired
	private Cache myVideoCache;

	/**
	 * 由繼承class Override ex: @Value("true")
	 * 
	 * @Override public void setUseCache(boolean useCache) {
	 *           super.setUseCache(useCache); }
	 */
	private boolean useCache = false;

	/**
	 * 由繼承class Override ex: @Value("5")
	 * 
	 * @Override public void setFlushInterval(int flushInterval) {
	 *           super.setFlushInterval(flushInterval); }
	 */
	private double flushInterval; // 單位:分

	public void setUseCache(boolean useCache) {
		this.useCache = useCache;
	}

	public void setFlushInterval(double flushInterval) {
		this.flushInterval = flushInterval;
	}

	private boolean isGetCache = false; // 執行時是否有讀到cache，報表用

	/**
	 * 取得cache的主要邏輯: 1) cache有資料且status=0(成功) -> 直接從cache取得資料 2) 否則呼叫
	 * genResponseTmpData 建立xml資料再放入cache
	 */
	@SuppressWarnings("unchecked")
	public V findCache(HttpServletRequest request, HttpServletResponse response, boolean forceGenCache,
			Class<V> clazz) {
		V result = null;
		String key = genCacheKey(request);

		Element cacheElement = myVideoCache.get(key);
		isGetCache = false;

		if (cacheElement == null) {
			result = putCache(request, response, clazz, key);
		} else {
			result = (V) cacheElement.getObjectValue();
			log.debug("== get cache:" + key);
			isGetCache = true;
			if ((!"0".equals(result.getStatus().getCode())) // cache裏的status不為成功
					|| forceGenCache // 強制取得
			) {
				result = putCache(request, response, clazz, key);
				isGetCache = false;
			}
		}

		return result;
	}

	private V putCache(HttpServletRequest request, HttpServletResponse response, Class<V> clazz, String key) {
		V cacheValue = genResponseTmpData(request, response, clazz);
		Element element = new Element(key, cacheValue);
		element.setTimeToLive((int) (flushInterval * 60));
		myVideoCache.put(element);
		log.debug("== put cache:" + key);
		return cacheValue;
	}

	/**
	 * 產生cache的 key
	 */
	private String genCacheKey(HttpServletRequest request) {
		Enumeration<String> keys = request.getParameterNames();
		List<String> list = Collections.list(keys);
		Collections.sort(list); // 確保cache key的名稱一致，將params names做排序
		String str = "";
		String apiName = findAPIName(request);
		StringBuilder sb = new StringBuilder(apiName);
		sb.append("__");
		String[] notLogParamArray = { "chl", "chk", "forceGenCache" };
		String[] allowParamArray = allowParamArray();
		String[] notCacheParamArray = notCacheParamArray();
		for (String key : list) {
			if (ArrayUtils.contains(allowParamArray, key)) {
				sb.append(key + "_" + request.getParameter(key) + "__");
			} else {
				if (!(ArrayUtils.contains(notLogParamArray, key) || ArrayUtils.contains(notCacheParamArray, key))) {
					log.warn("not allow params:" + apiName + "/" + key + "=" + request.getParameter(key) + " | from:"
							+ request.getParameter("chl"));
				}
			}
			if (sb.length() > 2) {
				str = sb.substring(0, sb.length() - 2);
			} else {
				str = sb.toString();
			}
		}
		return str;
	}

	/**
	 * 允許且被拿來當cache key的參數
	 */
	abstract String[] allowParamArray();

	/**
	 * 允許且但"不"拿來當cache key的參數
	 */
	String[] notCacheParamArray() {
		return new String[] {};
	}

}
