package com.wrel.apiservice.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wrel.apiservice.IFindRegistrationService;
import com.wrel.dao.IRegistrationDAO;
import com.wrel.vo.output.FindRegistrationData;
import com.wrel.vo.output.RegistrationData;

@Service("findRegistrationService")
public class FindRegistrationService extends BaseAPIService<RegistrationData, FindRegistrationData>
		implements IFindRegistrationService {

	@Autowired
	private IRegistrationDAO registrationDAO;

	@Override
	RegistrationData findData(HttpServletRequest request) throws Exception {
		RegistrationData data = new RegistrationData();
		List<Map<String, Object>> list = registrationDAO.findRegistrationByUserId("1");
		if (list.size() > 0) {
			Map<String, Object> map = list.get(0);
			System.out.println(map);
			String userId = String.valueOf(map.get("userId"));
			data.setUserId(userId);
		}
		return data;
	}

	@Override
	String[] allowParamArray() {
		String[] allowParamArray = { "" };
		return allowParamArray;
	}

}
